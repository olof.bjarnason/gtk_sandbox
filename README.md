
A repository of GTK 3 tests.

Each sub-directory contains a test with a README.

##gtkmm_action

An experiment in using GActions.

Four actions are defined:

1. An action that triggers an event.
2. An action that toggles a value.
3. An action that selects an integer.
4. An action that selects a string.

A GUI is built from an XML .ui file.

Each action is exercised in a variety of way (triggering console output):

1. Internally via code (before GUI is constructed.
2. In an application menu. Actions 3 and 4 are presented two different ways, once as a set of radial buttons and once as a drop-down menu.
3. In a toolbar (ToolButton, ToggleToolButton, RadioToolButton RadioToolButton).
4. As a normal GTK items (Button, ToggleButton, RadioButton, RadioButton).

Labels and separators are also used. 

Adding a SpinButton works if added via code. Fails if added via XML. The SpinButton is not useful in "menu" mode (when falling of the end of toolbar).

The bottom part of the window contains an attempt to mix GtkActions and GActions which fails as test-application.ui must be read to create the ActionGroup before the action can be added.


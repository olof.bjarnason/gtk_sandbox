

#include "test-application.h"

#include <gtk/gtk.h>

#include <iostream>

// ------------------------

TestApplication::TestApplication()
  : Gtk::Application("org.tavmjong.testapplication")
{
  Glib::set_application_name("Test Application");
}

Glib::RefPtr<TestApplication> TestApplication::create()
{
  return Glib::RefPtr<TestApplication>(new TestApplication());
}

void
TestApplication::on_startup()
{

  Gtk::Application::on_startup();

  // Add actions
  m_test1 = add_action(              "test1",sigc::mem_fun(*this, &TestApplication::on_test1) );
  m_test2 = add_action_bool(         "test2",sigc::mem_fun(*this, &TestApplication::on_test2), false);
  m_test3 = add_action_radio_integer("test3",sigc::mem_fun(*this, &TestApplication::on_test3), 0);
  m_test4 = add_action_radio_string( "test4",sigc::mem_fun(*this, &TestApplication::on_test4), "Mango");
  Glib::VariantType Double(G_VARIANT_TYPE_DOUBLE);
  m_test5 = add_action_with_parameter("test5",Double,sigc::mem_fun(*this,&TestApplication::on_test5));

  // Excercise actions (don't need GUI for this).
  activate_action("test1");
  activate_action("test2");
  int x = 2;
  activate_action("test3", Glib::Variant<gint32>::create(x));
  Glib::ustring s("Banana");
  activate_action("test4", Glib::Variant<Glib::ustring>::create(s));
  activate_action("test5", Glib::Variant<double>::create(2.71) );

  // Construct UI (see Gio::Menu to construct by hand)
  m_refBuilder = Gtk::Builder::create();

  try
    {
      m_refBuilder->add_from_file("test-application.ui");
    }
  catch (const Glib::Error& err)
    {
      std::cerr << "Building UI failed: " << err.what();
    }

  // Add menus to application.
  auto object = m_refBuilder->get_object("menu-bar");
  auto menu = Glib::RefPtr<Gio::Menu>::cast_dynamic(object);
  if (!menu) {
    std::cerr << "Menu not found" << std::endl;
  } else {
    // These should use different 'menu' as they have different purposes:
    set_menubar( menu );  // Per window menus
    set_app_menu( menu ); // Application menus
  }
}

void
TestApplication::on_activate()
{
  std::cout << "TestApplication::on_activate()" << std::endl;
  create_window(); 
}

void TestApplication::on_test1()
{
  std::cout << "on_test1" << std::endl;
}

void TestApplication::on_test2()
{
  bool active = false;
  m_test2->get_state(active);

  active = !active;

  m_test2->change_state(active);

  if (active) {
    std::cout << "on_test2: active" << std::endl;
  } else {
    std::cout << "on_test2: not active" << std::endl;
  }
}

void TestApplication::on_test3( int value )
{
  std::cout << "on_test3: Entrance" << std::endl;
  static bool block = false;
  if (block) return;
  block = true;

  m_test3->change_state( value );

  block = false;

  std::cout << "on_test3: " << value << std::endl;
}

void TestApplication::on_test4( Glib::ustring value )
{
  static bool block = false;
  if (block) return;
  block = true;

  m_test4->change_state( value );

  block = false;

  std::cout << "on_test4: " << value << std::endl;
}

void TestApplication::on_test5( Glib::VariantBase value )
{
  // No state
  Glib::Variant<double> d = Glib::VariantBase::cast_dynamic<Glib::Variant<double> > (value);
  std::cout << "on_test5: " << d.get() << std::endl;
}

void TestApplication::on_test5_wrap()
{
  std::cout << "on_test5_wrap" << std::endl;
  double value = m_adjustment0->get_value();
  // Do some validiation...
  activate_action("test5", Glib::Variant<double>::create(value) );
}

void
TestApplication::create_window()
{
  std::cout << "TestApplication::create_window()" << std::endl;
  auto window = new Gtk::ApplicationWindow();

  add_window( *window );  // Application runs till window is closed.
  window->set_title("Test Application");

  window->signal_hide().connect(
    sigc::bind<Gtk::Window*>(sigc::mem_fun(*this, &TestApplication::on_window_hide), window)
  );

  // Gtk::ApplicationWindow is derived from Gtk::Bin so it can contain only one child.
  auto box = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_VERTICAL));
  window->add(*box);


  // Actions in Tool Bar ------------------------------------

  Gtk::Toolbar* toolbar = nullptr;
  m_refBuilder->get_widget("toolbar", toolbar);
  if (!toolbar) {
    std::cerr << "Toolbar not found" << std::endl;
    return;
  }

  box->add(*toolbar);

  // Add targets to buttons defined in XML (how to do this in XML?).
  // This generates a Gtk-WARNING **:
  // actionhelper: action app.test3 can't be activated due to a parameter type mismatch
  // but it does seem to work.
  Gtk::RadioToolButton* rtb0 = nullptr;
  m_refBuilder->get_widget("button_test3_0", rtb0);
  if (!rtb0) {
    std::cerr << "RadioToolBar 0 not found" << std::endl;
  } else {
    gtk_actionable_set_action_target_value( GTK_ACTIONABLE(rtb0->gobj()), g_variant_new_int32(0) );
  }

  Gtk::RadioToolButton* rtb1 = nullptr;
  m_refBuilder->get_widget("button_test3_1", rtb1);
  if (!rtb1) {
    std::cerr << "RadioToolBar 1 not found" << std::endl;
  } else {
    gtk_actionable_set_action_target_value( GTK_ACTIONABLE(rtb1->gobj()), g_variant_new_int32(1) );
  }

  Gtk::RadioToolButton* rtb2 = nullptr;
  m_refBuilder->get_widget("button_test3_2", rtb2);
  if (!rtb2) {
    std::cerr << "RadioToolBar 2 not found" << std::endl;
  } else {
    gtk_actionable_set_action_target_value( GTK_ACTIONABLE(rtb2->gobj()), g_variant_new_int32(2) );
  }

  Gtk::RadioToolButton* rtbs0 = nullptr;
  m_refBuilder->get_widget("button_test4_0", rtbs0);
  if (!rtbs0) {
    std::cerr << "RadioToolBar s0 not found" << std::endl;
  } else {
    gtk_actionable_set_action_target_value( GTK_ACTIONABLE(rtbs0->gobj()), g_variant_new_string("Banana") );
  }

  Gtk::RadioToolButton* rtbs1 = nullptr;
  m_refBuilder->get_widget("button_test4_1", rtbs1);
  if (!rtbs1) {
    std::cerr << "RadioToolBar s1 not found" << std::endl;
  } else {
    gtk_actionable_set_action_target_value( GTK_ACTIONABLE(rtbs1->gobj()), g_variant_new_string("Mango") );
  }

  // A Label Tool Item
  Gtk::ToolItem* item0 = nullptr;
  m_refBuilder->get_widget("tool_item_0", item0);
  if (!item0) {
    std::cerr << "Item item0 not found" << std::endl;
  } else {

    // Add widget
    auto label0 = Gtk::manage(new Gtk::Label("MyLabel: A very, very, very, very, very, very, very, long label to push the buttons off the end of the toolbar.") );
    item0->add(*label0);
  }

  // Another Label Tool Item, with overflow Menu
  Gtk::ToolItem* item1 = nullptr;
  m_refBuilder->get_widget("tool_item_1", item1);
  if (!item1) {
    std::cerr << "Item item1 not found" << std::endl;
  } else {

    // Add widget
    auto label1 = Gtk::manage(new Gtk::Label("MyLabel at end") );
    item1->add(*label1);

    // Add overflow menu item
    auto menu1 = Gtk::manage(new Gtk::MenuItem("MyLabel at end"));
    item1->set_proxy_menu_item("MyLabel", *menu1);
  }


  // A SpinButton
  m_adjustment0 = Gtk::Adjustment::create(50, 0, 100, 1, 10, 0);
  m_adjustment0->signal_value_changed().connect(
    sigc::mem_fun(*this, &TestApplication::on_test5_wrap));

  Gtk::ToolItem* item2 = nullptr;
  m_refBuilder->get_widget("tool_item_2", item2);
  if (!item2) {
    std::cerr << "Item item2 not found" << std::endl;
  } else {

    // Add widget
    auto spin0 = Gtk::manage(new Gtk::SpinButton(m_adjustment0));
    item2->add(*spin0);

    // Add overflow menu item
    auto menu2 = Gtk::manage(new Gtk::MenuItem());
    auto spin1 = Gtk::manage(new Gtk::SpinButton(m_adjustment0));
    menu2->add(*spin1);
    item2->set_proxy_menu_item("MySpin", *menu2);
  }


  // Actions as Buttons --------------------------------------

  auto button = Gtk::manage(new Gtk::Button("Test1"));
  // Gtk::Button not derived from Gtk::Actionable due to ABI issues. Must use Gtk. Fixed in Gtk4.
  // Search stackoverflow for: ABI gtk_actionable_set_action_name
  gtk_actionable_set_action_name( GTK_ACTIONABLE(button->gobj()), "app.test1");
  box->add(*button);


  auto toggle = Gtk::manage(new Gtk::ToggleButton("Test2"));
  gtk_actionable_set_action_name( GTK_ACTIONABLE(toggle->gobj()), "app.test2");
  box->add(*toggle);


  auto radio0 = Gtk::manage(new Gtk::RadioButton("Test3: 0"));
  auto group  = radio0->get_group();
  auto radio1 = Gtk::manage(new Gtk::RadioButton(group, "Test3: 1"));
  auto radio2 = Gtk::manage(new Gtk::RadioButton(group, "Test3: 2"));

  gtk_actionable_set_action_name( GTK_ACTIONABLE(radio0->gobj()), "app.test3");
  gtk_actionable_set_action_name( GTK_ACTIONABLE(radio1->gobj()), "app.test3");
  gtk_actionable_set_action_name( GTK_ACTIONABLE(radio2->gobj()), "app.test3");

  gtk_actionable_set_action_target_value( GTK_ACTIONABLE(radio0->gobj()), g_variant_new_int32(0) );
  gtk_actionable_set_action_target_value( GTK_ACTIONABLE(radio1->gobj()), g_variant_new_int32(1) );
  gtk_actionable_set_action_target_value( GTK_ACTIONABLE(radio2->gobj()), g_variant_new_int32(2) );

  auto box3 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL));
  box3->add(*radio0);
  box3->add(*radio1);
  box3->add(*radio2);
  box->add(*box3); // This causes the action value to be set to 0.

  auto radios0 = Gtk::manage(new Gtk::RadioButton("Banana"));
  auto groups  = radios0->get_group();
  auto radios1 = Gtk::manage(new Gtk::RadioButton(groups, "Mango"));

  gtk_actionable_set_action_name( GTK_ACTIONABLE(radios0->gobj()), "app.test4");
  gtk_actionable_set_action_name( GTK_ACTIONABLE(radios1->gobj()), "app.test4");

  gtk_actionable_set_action_target_value( GTK_ACTIONABLE(radios0->gobj()), g_variant_new_string("Banana") );
  gtk_actionable_set_action_target_value( GTK_ACTIONABLE(radios1->gobj()), g_variant_new_string("Mango") );

  auto box4 = Gtk::manage(new Gtk::Box(Gtk::ORIENTATION_HORIZONTAL));
  box4->add(*radios0);
  box4->add(*radios1);
  box->add(*box4);

  // Menu via GtkAction
  m_beep = Beep::create("beep", "format-justify-center");
  Glib::RefPtr< Glib::Object > actgrp = m_refBuilder->get_object("actiongroup");
  //  Gtk::ActionGroup* actgrp = m_refBuilder->get_object("actiongroup");
  if (!actgrp) {
    std::cerr << "Item actiongroup not found" << std::endl;
  } else {
    Glib::RefPtr< Gtk::ActionGroup >::cast_static(actgrp)->add(m_beep);
    std::cerr << "Added 'beep' to group" << std::endl;
  }

  Gtk::MenuBar* bar0 = nullptr;
  m_refBuilder->get_widget("menubar0", bar0);
  if (!bar0) {
    std::cerr << "Item bar0 not found" << std::endl;
  } else {
    box->add(*bar0);
  }
  
  // Toolbar via GtkAction
  Gtk::Toolbar* tool0 = nullptr;
  m_refBuilder->get_widget("toolbar0", tool0);
  if (!tool0) {
    std::cerr << "Item tool0 not found" << std::endl;
  } else {
    box->add(*tool0);
  }

  Gtk::ToolItem* beep0 = nullptr;
  m_refBuilder->get_widget("beep0", beep0);
  if (!beep0) {
    std::cerr << "Item beep not found" << std::endl;
  }

  Glib::RefPtr< Glib::Object > hmm = m_refBuilder->get_object("beep0");
  if (!hmm) {
    std::cerr << "Item hmm not found (Object)" << std::endl;
  }

  window->show_all();
  std::cout << "TestApplication::create_window(): Exit" << std::endl;
}

void
TestApplication::on_window_hide(Gtk::Window* window)
{
  std::cout << "TestApplication::on_window_hide()" << std::endl;
  delete window;
}

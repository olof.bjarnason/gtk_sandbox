#ifndef TOOL_LABEL_H
#define TOOL_LABEL_H

#include <gtkmm.h>

class ToolLabel:: public Gtk::ToolItem
{
 public:
  ToolLabel(Glib::ustring label) : label(_label) {};
 private:
  Glib::ustring _label;
};

#endif // TOOLBAR_LABEL_H


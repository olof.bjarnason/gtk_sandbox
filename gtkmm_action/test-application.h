#ifndef TEST_APPLICATION_H
#define TEST_APPLICATION_H

#include <gtkmm.h>
#include <iostream>

// A Gtk::Action for testing using old ui.
class Beep : public Gtk::Action {

public:

  static Glib::RefPtr<Beep> create( const Glib::ustring &name,
                       const Glib::ustring &icon_name,
                       const Glib::ustring &label = Glib::ustring(),
                       const Glib::ustring &tooltip = Glib::ustring()) {
    return Glib::RefPtr<Beep>(new Beep(name, icon_name, label, tooltip));
  }

  Beep( const Glib::ustring &name,
        const Glib::ustring &icon_name,
        const Glib::ustring &label,
        const Glib::ustring &tooltip)
    : Gtk::Action(name, icon_name, label, tooltip) {
    std::cout << "Constructing Beep" << std::endl;
  }
};

// ------------------------


class TestApplication : public Gtk::Application
{
 protected:
  TestApplication();

 public:
  static Glib::RefPtr<TestApplication> create();

 protected:
  void on_startup()  override;
  void on_activate() override;

  void on_test1();
  void on_test2();
  void on_test3( int value );
  void on_test4( Glib::ustring value );
  void on_test5( Glib::VariantBase value );

  void on_test5_wrap();

 private:
  void create_window();
  void on_window_hide(Gtk::Window* window);


  Glib::RefPtr<Gtk::Builder> m_refBuilder;

  Glib::RefPtr<Gio::SimpleAction> m_test1;
  Glib::RefPtr<Gio::SimpleAction> m_test2;
  Glib::RefPtr<Gio::SimpleAction> m_test3;
  Glib::RefPtr<Gio::SimpleAction> m_test4;
  Glib::RefPtr<Gio::SimpleAction> m_test5;
  Glib::RefPtr<Gtk::Adjustment> m_adjustment0;

  Glib::RefPtr<Beep> m_beep;

  Glib::RefPtr<Gtk::ActionGroup> m_action_group;
};

#endif // TEST_APPLICATION_H

